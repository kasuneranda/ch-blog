<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 15/5/19
 * Time: 3:49 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'title' => $this->post_title,
            'meta_title' => $this->post_meta_title,
            'meta_description' => $this->post_meta_description,
            'body' => $this->post_body,
            'slug' => $this->post_slug,
            'featured_type'=>$this->post_feature_media_type,
            'featured_media'=>$this->featured_media,
            'featured_image' => $this->featured_image,
            'featured_video'=>$this->post_video,
            'featured_video_thumb'=>$this->video_thumb,
            'visibility' => $this->getVisibility($this->post_visibility),
            'date'=>Carbon::parse($this->created_at)->diffForHumans(),

            'author'=> new AuthorResource($this->author),

            'categories'=>CategoryResource::collection($this->categories),
            'tags'=>CategoryResource::collection($this->tags),


        ];

        return $resource;
    }

    protected function getVisibility($visibility)
    {
        switch ($visibility) {
            case  1:
                return 'Visible to all';
                break;
            case 2:
                return 'Hidden to public';
                break;
            case 3:
                return 'Hidden to all';
                break;
        }

    }
}