<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 15/5/19
 * Time: 4:06 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{

    public function toArray($request)
    {
        $resource = [
            'id'=>$this->id,
            'name'=>$this->category_name
        ];

        return $resource;
    }
}