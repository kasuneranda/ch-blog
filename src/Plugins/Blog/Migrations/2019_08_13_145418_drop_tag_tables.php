<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTagTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //move existing tags to labels table
        if (Schema::hasTable('blog_tags')) {

            //tags
            $records = \DB::table('blog_tags')->get();
            if ($records->count() > 0) {
                foreach ($records as $record) {
                    $data = [
                        'model' => "BlogArticle",
                        'label' => $record->tag_name,
                        'color' => $record->tag_color ?? sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
                        'author' => \Auth::id() ?? 1
                    ];

                    $storedLabel = \DB::table('labels')->insertGetId($data);
                }
            }

            //tag relation
            $relations = \DB::table('article_tags')->get();
            if ($relations->count() > 0) {
                foreach ($relations as $relation) {

                    $data = [
                        'model' => "BlogArticle",
                        'model_id' => $relation->article_id,
                        'label_id' => \DB::table('labels')
                            ->where('model', "BlogArticle")
                            ->where('label', \DB::table('blog_tags')->find($relation->tag_id)->tag_name)
                            ->first()->id

                    ];

                    \DB::table('labels_relation')->insert($data);
                }
            }

            //drop tables
            Schema::dropIfExists('blog_tags');
            Schema::dropIfExists('article_tags');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
