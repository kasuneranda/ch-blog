<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 2/6/19
 * Time: 3:17 PM
 */

namespace Creativehandles\ChBlog\Http\Controllers\FrontendControllers;

use App\Helpers\Tools;
use App\Http\Controllers\Controller;
use Creativehandles\ChBlog\Plugins\Blog\Blog;
use Mcamara\LaravelLocalization\LaravelLocalization;

class FrontendBlogController extends Controller
{
    /**
     * @var Blog
     */
    private $blog;

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    //blog posts listing function
    public function index()
    {
        //fetch most recent article
        $firstArticle = $this->blog->getRecentArticles(null, ['tags', 'categories', 'author'])->sortByDesc('article_date')->first();

        if ($firstArticle) {
            $allArticles = $this->blog->getRecentArticlesExceptReadingOne('id', '!=',
                $firstArticle->id, null, ['tags', 'categories', 'author'])->sortByDesc('article_date');

            $slice = 4;
            $firstFourArticles = $allArticles->take($slice);
            $otherArticles = $allArticles->slice($slice);
        }

        return view('pages.blog.post-listing')->with(compact('firstArticle', 'firstFourArticles','otherArticles'));
    }


    //show single article on frontend
    public function show($id,$slug)
    {
        $postId = $id;
        $postSlug = $slug;

        $localization = app(LaravelLocalization::class);
        //fetch article by slug
        $article = $this->blog->getArticleById((int) $postId);

        if (!$article) {
            abort(404);
        }

        //get article language
        $articleLanguage = $article->post_language;

        //get request's language
        $requestLocale = request()->get('locale');

        //if request comes from the same server show the article's lang
        if(!$requestLocale){
            if ((isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))) {
                if (strtolower(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST)) == strtolower($_SERVER['HTTP_HOST'])) {
                    $requestLocale = $articleLanguage;
                }
            }else{
                //if request comes outside the server get browser's lang
                $requestLocale = request()->getLocale();
            }
        }

        //get sibling article languages
        $availableArticles =$article->siblingLanguageArticles();
        $availableLanguages = $availableArticles->pluck('post_language')->all();

        $translatedRoutes =[
            //base article language
            $article->post_language => \LaravelLocalization::getLocalizedUrl($article->post_language,Tools::getTranslatedRoute($article->post_language,route('preview-article',['id'=>$article->id,'slug'=>$article->post_slug]),[],true))
        ];

        foreach ($availableArticles as $singleArticle){
            $translatedRoutes[$singleArticle->post_language] = \LaravelLocalization::getLocalizedUrl($singleArticle->post_language,Tools::getTranslatedRoute($singleArticle->post_language,route('preview-article',['id'=>$singleArticle->id,'slug'=>$singleArticle->post_slug]),[],true),[],true);
        }

//
//        //if we have article in rquested language redirect it to that article if not just give requested one
//        if(in_array($requestLocale,$availableLanguages)){
//            //set app locale to article language
//            $article = $availableArticles->where('post_language',$requestLocale)->first();
//
//            $url = $localization->getLocalizedURL($requestLocale,route('preview-article',['id'=>$article->id,'slug'=>$article->post_slug]),[],true);
//            //redirect to the translated article
//           return redirect($url);
//        }
//
//        //if someone requesting for a translation while being in different locale,. it redirects to article's lang
//        if($localization->getCurrentLocale() != $article->post_language){
//            $url = $localization->getLocalizedURL($article->post_language,route('preview-article',['id'=>$article->id,'slug'=>$article->post_slug]),['locale'=>$article->post_language],true);
//            //redirect to the translated article
//            return redirect($url);
//        }
        //recent articles
        $recentArticles = $this->blog->getRecentArticlesExceptReadingOne('id', '!=', $postId, 2)->sortByDesc('article_date');
        return view('pages.blog.post-layout')->with(compact('article', 'recentArticles','translatedRoutes'));
    }

}