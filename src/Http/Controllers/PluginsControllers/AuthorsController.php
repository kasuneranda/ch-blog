<?php

namespace Creativehandles\ChBlog\Http\Controllers\PluginsControllers;

use App\Http\Controllers\AdminControllers\UsersController;
use App\Http\Controllers\APIControllers\CoreControllers\UsersController as ApiUsersController;
use Creativehandles\ChBlog\Plugins\Blog\Blog;
use Creativehandles\ChBlog\Plugins\Trainings\Models\InstructorModel;
use App\Repositories\CoreRepositories\Contracts\RoleRepositoryInterface;
use App\Repositories\CoreRepositories\Contracts\UserRepositoryInterface;
use App\Traits\UploadTrait;
use App\User;
use App\UserLevel;
use App\UserSocialProfiles;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Instructor;

use DB;
use Spatie\Permission\Models\Role;
use Validator;

class AuthorsController extends UsersController
{

    use UploadTrait;

    protected $views = 'Admin.Authors.';
    protected $logprefix = "AuthorsController";
    protected $backRoute = "admin.authors.index";
    /**
     * @var Blog
     */
    private $blog;
    /**
     * @var UserRepository
     */
    private $userRepository;


    public function __construct(
        ApiUsersController $apiController,
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository,
        Blog $blog
    ) {
        parent::__construct($apiController, $userRepository, $roleRepository);
        $this->blog = $blog;
        $this->userRepository = $userRepository;
    }


    public function grid(Request $request)
    {

        $searchColumns = ['users.email', 'user_details.first_name', 'user_details.last_name'];
        $orderColumns = ['1' => 'users.email', '2' => 'user_details.first_name'];
        $with = ['userDetails', 'role'];
        $where = ['users.role_id', '=', Role::findByName('Author')->id];
        $data = $this->userRepository->getDataforDataTables($searchColumns, $orderColumns, '2', $with, $where);

        $requiredUsers = config('users.required', []);    // need to disable deleting required users for the app

        $data['data'] = array_map(function ($item) use ($requiredUsers) {
            $return = [
                $item['id'],
                $item['email'],
                $item['user_details']['first_name'],
                $item['user_details']['last_name'],
                $item['avatar'] ?? null,
                $item['user_rank'] ?? ''
            ];
            $return[] = !in_array($return[2], $requiredUsers, true); // delete is allowed if not a required user

            return $return;
        }, $data['data']);

        echo json_encode($data);

    }


    public function update(Request $request)
    {
        $request->merge(['username' => $request->email, 'role_id' => $this->userRepository->getAuthorId()]);
        return parent::update($request);
    }

    public function store(Request $request)
    {
        $request->merge(['username' => $request->email, 'role_id' => $this->userRepository->getAuthorId()]);
        return parent::store($request);
    }



}
