@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/summernote/summernote-lite.css") }}">
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title mb-0">Blog</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Blog</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-header fixed-header">
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a href="{{route('createArticle')}}"><span class="ft-file-plus" style="color: green"></span></a>
                                    </li>
                                    <li>
                                        <a href="{{route('listArticles')}}"><span class="ft-layers" style="color: green"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card-body text-center">
                            <h4 class="card-title mb-1">Articles</h4>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-header fixed-header">
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a href="{{route('createCategory')}}"><span class="ft-file-plus" style="color: green"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card-body text-center">
                            <h4 class="card-title mb-1">Categories</h4>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("js/scripts/summernote/summernote-lite.js") }}" type="text/javascript"></script>

    <script>
        $(document).ready(function(){
            $('#postBody').summernote({
                height: 300,
                placeholder:"Post Body",
            });
        });
    </script>
@endsection