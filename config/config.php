<?php
/*
 * You can place your custom package configuration in here.
 */

$featureImageResolutions = json_decode(env('CH_BLOG_FEATURED_IMAGE_RESOLUTIONS'), true);

return [
    'fe_base_url' => env('FE_BASE_URL', ''),
    'fe_blog_url' => env('FE_BLOG_URL', ''),

    'image_optimization' => [
        'img_quality' => env('CH_BLOG_IMG_QUALITY', 90),
        'img_format' => env('CH_BLOG_IMG_FORMAT', 'webp'),
        'featured_image' => is_array($featureImageResolutions) ? $featureImageResolutions : [
            ['width' => 170, 'height' => 128], // admin_article_list
            ['width' => 300, 'height' => 200], // front_article_list
            ['width' => 1110, 'height' => 555]  // article_thumbnail
        ],
        'featured_image_default_size' => env('CH_BLOG_FEATURED_IMAGE_DEFAULT_RESOLUTION', '1110x555'),
    ]
];